///booksystem url_parser.js

///a module used to parse url to related deal page

///error deal module used to deal with error process but not error request
const error = require("./ERROR_DEAL");

function url_map(router, mapping){
    for(let url in mapping){
        if(url.toLowerCase().startsWith("get")){
            let path = url.substring(4);

            router.get(path, mapping[url]);
            console.log(`register ${url}`);
        }
        else if(url.toLowerCase().startsWith("post")){
            let path = url.substring(5);

            router.post(path, mapping[url]);
            console.log(`register ${url}`);
        }
        else if(url.toLowerCase().startsWith("put")){
            let path = url.substring(4);
    
            router.put(path, mapping[url]);
            console.log(`register ${url}`);
        }
        else if(url.toLowerCase().startsWith("delete")){
            let path = url.substring(7);

            router.del(path, mapping[url]);
            console.log(`register ${url}`);
        }
        else{
            console.log(`invalide url`);
        }
    }
}


function map_control(router, dir){
    const fs = require("fs");
    fs.readdir(__dirname + '/' + dir, function(err, files){
        if(err){
            console.log("url deal file read failed");
            error.ERROR_DEAL();
        }

        files.filter(function(file){
            return file.endsWith('.js');
        }).forEach(function(file){
            let mapping = require(__dirname + '/' + dir + '/' + file);
            url_map(router, mapping);
        });
    });
}


module.exports = function(dir){
    let url_dir = dir || 'url';
    const router = require("koa-router")();
    
    map_control(router, url_dir);
    return router.routes();
};   

