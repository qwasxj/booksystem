///booksystem formate formate.js


let format_date = function(){
    let date = new Date();
    
    let year = date.getFullYear();
    let month = date.getMonth();
    let day = date.getDate();
    
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let secondes = date.getSeconds();

    console.log(`${year}-${month}-${day}  ${hours}:${minutes}:${secondes}`);
}


module.exports ={
    'format_date': format_date,
}
