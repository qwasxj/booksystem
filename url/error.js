///booksystem url error.js

/// deal_page module used to get page in error request
const deal_page = require("./oprate_function/deal_page");

///path module used to get the page path
const path_mod = require("path");

let fn_error = async function(ctx, next){
    let path = path_mod.resolve(BOOKSYSTEM, "./page/page_error.html");

    ctx.response.type = "text/html";
    ctx.response.body = await deal_page.get_page(path);
}


module.exports = {
    "GET /error": fn_error,
    "get /error": fn_error,
    "POST /error": fn_error,
    "post /error": fn_error,
}