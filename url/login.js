///booksystem url login.js
///used to deal with login request

///used to find request page path
const path_mod = require("path");

/// deal_page module used to read response file sent to client
const deal_page = require("./oprate_function/deal_page");

///used to check loginer is exist or not
const sql = require("./oprate_function/sql");

///used to checklogin include name and password

async function check(ctx){
    // return number stand for check result 
    // 0: user name not exist
    // 1: password error
    // 2: righg
    let user_password = ctx.request.body.userPassword;
    let user_name = ctx.request.body.userName;

    console.log(ctx.request.body);
    let str = "select * from user_info where user_number = '" + user_name +"'";

    let results = await sql.query(str, []);
    if(results.length == 0)
        return 0;
    if(results[0].user_password != user_password)
        return 1;
    ctx.session.is_admin = results[0].is_admin;
    ctx.session.userName = results[0].user_number;
    return 2;
}


///deal with login
async function login(ctx){
    ///status 2: right
    ///status 1: password error
    ///status 0: not exsit
    let status = 0;
    let path = "";

    status = await check(ctx);
    
    // console.log("I am " + ctx.session.is_admin);
    if(status == 2){
        //console.log(ctx.session.is_admin);
        if(ctx.session.is_admin)
            path = path_mod.resolve(BOOKSYSTEM, "./page/page_admin_search.html");
        else
            path = path_mod.resolve(BOOKSYSTEM, "./page/page_user_search.html");
    }
    else if(status ==  1){
        path = path_mod.resolve(BOOKSYSTEM, "./page/page_user_login_error_password.html");
    }else{
        path = path_mod.resolve(BOOKSYSTEM, "./page/page_user_login_again.html");
    }

    ctx.response.type = "text/html";
    ctx.response.body = await deal_page.get_page(path);
}


let fn_uLogin = async function(ctx, next){
    await login(ctx);
}

module.exports = {
    "post /userLogin": fn_uLogin,
    "POST /userLogin": fn_uLogin
};