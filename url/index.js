///booksystem url index.js

///deal with request index page/user login poage/administrator login page

const path_mod = require('path');
const deal_page = require("./oprate_function/deal_page");


///deal with request index page
let fn_index = async function(ctx, next){
    let path = path_mod.resolve(BOOKSYSTEM, "./page/page_index.html");
    
    ctx.response.type = "text/html";
    ctx.response.body = await deal_page.get_page(path);
}


///deal with request user login page
let fn_uLogin = async function(ctx, next){
    let path = path_mod.resolve(BOOKSYSTEM, "./page/page_user_login.html");
    
    ctx.response.type = "text/html";
    ctx.response.body = await deal_page.get_page(path);
}


module.exports = {
    "GET /": fn_index,
    "get /": fn_index,
    "GET /userLogin": fn_uLogin,
    "get /userLogin": fn_uLogin,
};