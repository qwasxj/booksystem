///booksystem url user_oprate.js

/// module used to manage user request 

const sql = require("./oprate_function/sql");

/// common user opration
/// return lend book  refer to table user_info book_info
/// modify one's password refer to user_info
let fn_query_my_book = async function(ctx, next){
    let info ="";

    let user_number = ctx.session.userName;
    let str = "select * from lend_info where user_number = ?";
    let values = [user_number];
    let lend_info = await sql.query(str, values);

    if(lend_info.length == 0)
        info = "no lend book info!";
    else
        info = "here are some lend books!";
     
     ctx.response.body = {
        info: info,
        results: lend_info
    }
};

let fn_return = async function(ctx, next){
    let info = "";

    let book_number = ctx.request.body.bookName;
    let user_number = ctx.session.userName;

    let str = "select * from lend_info where user_number = ? and book_number = ?";
    let values = [book_number, user_number];
    let lend_info = await sql.query(str, values);

    console.log(user_number);
    console.log(book_number);
    console.log(lend_info);
    console.log(lend_info.length);
    if(lend_info.length == 0)
       info = "no lend info, return failed!!";
    else if(lend_over_time(lend_info))
        info = "return over time";
    else{
        let left_amount = lend_info[0].left_amount;
        let total_amount = lend_info[0].total_amount;
        str = "delete from lend_info where user_number = ?";
        values = [user_number];
        let results = await sql.delete(str, values);
        if(results.affectedRows == 0)
            info = "delete failed!!";
        else{
            str = "update book_info set letf_amount = ? where total_amount = ?";
            values = [left_amount + 1, total_amount + 1];
            await sql.update(str, values);
            info = "return book succed!!";
        }
    }

    ctx.response.body = {
        info: info
    }
};

let fn_lend = async function(ctx, next){
    let info = "";

    let book_number = ctx.request.body.bookName;
    let user_number = ctx.session.userName;

    let str = "select * from lend_info where user_number = ? and book_number = ?";
    let values = [book_number, user_number];
    let lend_info = await sql.query(str, values);

    str = "select * from book_info where book_number = ?";
    values = [book_number];
    let book_info = sql.query(str, values);
    if(book_info.length == 0)
        info = "book not exist, lend book failed";
    else{
        let left_amount = book_info[0].left_amount;
        let total_amount = book_info[0].total_amount;

        if(lend_info.length != 0)
            info = "lend failed,,you have lend this book!!";
        else if(lend_over_time(lend_info))
            info = "lend failed,you have some books lend over time, please return these books!!";
        else if(left_amount == 0)
            info = "lend failed, no this book left, sorry!!";
        else{
            str = "insert into lend_info(user_number, book_number, lend_time) values(?, ?, ?)";
            values = [user_number, book_number, new Date().toString()];
            await sql.insert(str, values);

            str = "update book_info set letf_amount = ? where total_amount = ?";
            values = [left_amount - 1, total_amount - 1];
            await sql.update(str, values);

            info = "lend succed!"; 
        }
    }

     ctx.response.body = {
        info: info
    }
};

let fn_modify_password = async function(ctx, next){
    let info = "";

    let new_password = ctx.request.body.newPassword;
    let new_password_again = ctx.request.body.newPasswordAgain;
    let user_number = ctx.session.userName;

    if(new_password != new_password_again)
        info = "two password is not equal!!";
    else{
        let str = "update user_info set user_password = ? where user_number = ?";
        values = [new_password, user_number];
        await sql.update(str, values);
        info = "modify password suceed!!";
    }

    ctx.response.body = {
        info: info
    }
};


/// admin opration
/// modify book_info refer to table book_info
/// opration update insert select
/// modify common user info refer to table user_info book_info

let fn_add_book = async function(ctx, next){
    let info = "";

    let left_amount = ctx.request.body.leftAmount;
    let total_amount = ctx.request.body.totalAmount;
    let book_name = ctx.request.body.bookName;
    let book_number = ctx.request.body.book_number;
    let is_admin = ctx.session.is_admin;

    if(!is_admin){
        info = "you do not have the priority to add book!";
    }
    else{
        let str = "select * from book_info where book_number = ?";
        let values = [book_number];
        let book_info = await sql.query(str, values);

        if(book_info.length == 0){
            str = "insert into book_info values(?, ?, ?, ?)";
            values = [book_number, book_name, left_amount, total_amount];
            await sql.insert(str, values);
        }
        else{
            str = "update book_info set left_amount = ?, total_amount = ? where book_number = ?";
            let oleft_amount = book_info[0].left_amount;
            let ototal_amount = book_info[0].total_amount;
            values = [oleft_amount + left_amount, ototal_amount + total_amount];
            await sql.update(str, values);
        }
        info = "add book succed!!";
    }

     ctx.response.body = {
        info: info
    }
};

let fn_modify_user_password = async function(ctx, next){
    let info = "";

    let user_password = ctx.request.body.password;
    let user_number = ctx.request.body.userName;
    let is_admin = ctx.session.is_admin;

    if(!is_admin)
        info = "you do  not have the priority to change user password!!";
    else{
        let str = "update user_info set user_password = ? where user_number = ?";
        values = [user_password, user_number];
        await sql.update(str, values);
        info = "change password succed!!";    
    }

    ctx.response.body = {
        info: info
    }
};

let fn_add_user = async function(ctx, next){
    let info = "";

    let user_number = ctx.request.body.userName;
    let user_password = ctx.request.body.userPassword;
    let admin = ctx.request.body.isAdmin;
    let is_admin = ctx.session.is_admin;

    if(!is_admin){
        info = "you do not have the priority to add user!!";
    }
    else{
        let str = "select * from user_info where user_number = ?";
        let values = [user_number];
        let user_info = await sql.query(str, values);
        if(user_info.length != 0){
            let str = "update user_info set user_password = ?, is_admin = ? where user_number = ?";
            let values = [user_password, admin, user_number];
            await sql.update(str, values);
            info = "add user succed!!";
        }
        else{
            let str = "insert into user_info values(?, ?, ?)";
            values = [user_number, user_password, admin];
            await sql.insert(str, values);
            info ="add user succed!!";
        }
    }

    ctx.response.body = {
        info: info
    }
}

let fn_delete_user = async function(ctx, next){
    let info= "";

    let user_number = ctx.request.body.userName;
    let is_admin = ctx.session.is_admin;
    console.log(is_admin);
    if(!is_admin)
        info = "you do not have the priority to delete user!!";
    else{
        let str = "select * from user_info where user_number = ?";
        let values = [user_number];
        let user_info = await sql.query(str, values);
        if(user_info.length == 0)
            info = "delete error! user is not exist!!";
        else{
            str = "delete from user_info where user_number = ?";
            values = [user_number];
            await sql.delete(str, values);
            info = "delete user succed!!";
        }
    }

     ctx.response.body = {
        info: info
    }
}

/// caculate if exists lend book time over 30 days
function lend_over_time(results){
    for(let element in results){
        let lend_time = results[element].lend_time;
        if(time_test(lend_time))
            return true;
    }
    return false;
}

///test whether time is over 30 day
function time_test(str){
    let now = new Date();
    let time_elements = str.split('-');
    let lend_time = new Date(time_elements[0], time_elements[1], time_elements[2], time_elements[3], time_elements[4], time_elements[5]);
    console.log(lend_time);
    let time_diff = now - lend_time;
    if(time_diff > 30 * 24 * 60 * 60 * 1000)
        return true;
    else
        return false;
}

/// get mysql time str
function format_time(date){
    let array = [];
    array.push(date.getFullYear());
    array.push(month = date.getMonth());
    array.push(date.getDate());
    array.push(date.getHours());
    array.push(date.getMinutes());
    array.push(date.getSeconds());
    return array.join('-');
}

// function to_JSON(info, results){
//     let JSON = "\"{";
//     for(let element in results){

//     }
//     return 
// }

module.exports = {
    "get /query_my_book": fn_query_my_book,
    "post /return_book": fn_return,
    "post /lend_book": fn_lend,
    "post /modify_password": fn_modify_password,
    
    "post /add_book": fn_add_book,
    "post /add_user": fn_add_user,
    "post /modify_book": fn_add_book,
    "post /delete_user": fn_delete_user,
    "post /modify_user_password": fn_modify_user_password,

    "get /query_my_book": fn_query_my_book,
    "POST /return_book": fn_return,
    "POST /lend_book": fn_lend,
    "POST /modify_password": fn_modify_password,
    
    "POST /delete_user": fn_delete_user,
    "POST /add_book": fn_add_book,
    "POST /modify_book": fn_add_book,
    "POST /add_user": fn_add_user,
    "POST /modify_user_password": fn_modify_user_password,
};