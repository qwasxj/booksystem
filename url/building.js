///booksystem url building.js

const deal_page = require("./oprate_function/deal_page");
const path_mod = require("path");

let fn_building = async function(ctx, next){
    let path = path_mod.resolve(BOOKSYSTEM, "./page/building.html");

    ctx.response.type = "text/html";
    ctx.response.body = await deal_page.get_page(path);
}



module.exports = {
    "GET /building": fn_building,
    "get /building": fn_building,
    "post /building": fn_building,
    "POST /building": fn_building,
};