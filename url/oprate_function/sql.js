///booksystem url oprate_function sql.js
/// used to deal with event related to database

const mysql = require("mysql");

let CON;

///excute database oporation 
///insert
///update
///delete
async function excute(str, values){
    return new Promise(function(resolve, reject){
        CON.query(str, values, function(error, results){
            if(error){
                console.log("error occur when excute database");
                return reject([]);
            }
            else{
                return resolve(results);
            }
        });
    });
}


///connect database
let fn_sql_connect = function(){
    console.log("mysql excuted!");
    let connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '123',
        database: 'booksystem'
    });

    connection.connect();
    CON = connection;
    console.log("database connect sucessed!");
};


///cloase database
let fn_sql_close = async function(){
    CON.end();
};


///query
let fn_sql_query = function(str, values)
{   
    return new Promise(function(resolve, reject){
        CON.query(str, values, function(error, results, fields){
            if(error){
                console.log("error occur when excute query!!");
                return reject([]);
            }
            else{
                return resolve(results);
            }
        });
    });
};

///insert
let fn_sql_insert = async function(str, values){
    let results = CON.query(str, values);
    
    //excepte key word duplicate
    if(results.length == 0)
        return excute(str, values);
    else
        console.log("insert element duplicate!!");
};

///update
let fn_sql_update = async function(str, values){
    return excute(str, values);
};

///delete
let fn_sql_delete = async function(str, values){
    return excute(str, values);
}



module.exports = {
    "connect": fn_sql_connect,
    "close": fn_sql_close,
    "query": fn_sql_query,
    "insert": fn_sql_insert,
    "update": fn_sql_update,
    "delete": fn_sql_delete
};