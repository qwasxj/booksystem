//test_sql.js

const mysql = require("mysql");

let connection = mysql.createConnection({
    host:"localhost",
    user: "root",
    password: "123",
    database: "booksystem"
});

connection.connect();
query_test();
connection.end();

function query_test(){
    let book_number = '0000000001';
    let user_number = '2014301500001';
    let str = "select * from lend_info where user_number = ? and book_number = ?";
    let values = [book_number, user_number];
    connection.query(str, values, function(error, results, fields){
        if(error) throw error;
        console.log(results);
    });
}

function querya_test(){
    let values = ['2014301500001'];
    connection.query("select * from user_info", [], function(error, results, fields){
        if(error) throw error;
        console.log(results);
    });
}

function delete_test(){
    let values = [];
    connection.query("delete from user_info where user_number = '2014301500005'", values, function(error, results){
        if(error) throw error;
        console.log(results);
    });
}

function insert_test(){
    let str = "insert into user_info values(?, ?, ?)";
    let values = ['2014301500012','qwas123456', false];
    connection.query(str, values, function(error, results){
        if(error) throw error;
        console.log(results);
    });
}

function update_test(){
    let values = ['2014301500001','qwas'];
    connection.query("update user_info set user_password = ? where user_number = ?", values, function(error, results){
        if(error) throw error;
        console.log(results);
    });
}

function time_test(){
    let now = new Date();
    str = "2017-4-1-3-2-1";
    let time_elements = str.split('-');
    // for(let element in time_elements)
    //     console.log(time_elements[element]);
    let lend_time = new Date(time_elements[0], time_elements[1], time_elements[2], time_elements[3], time_elements[4], time_elements[5]);
    //console.log(lend_time);
    let time_diff = now - lend_time;
    console.log(time_diff + " " + 30 * 24 * 60 * 60);
    console.log(now.getFullYear());
    if(time_diff > 30)
        return true;
    else
        return false;
}

/// get mysql time str
function format_time(date){
    let array = [];
    array.push(date.getFullYear());
    array.push(month = date.getMonth());
    array.push(date.getDate());
    array.push(date.getHours());
    array.push(date.getMinutes());
    array.push(date.getSeconds());
    return array.join('-');
}


function format_time_test(){
    let now = new Date();
    console.log(format_time(now));
}