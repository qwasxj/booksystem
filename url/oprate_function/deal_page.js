///booksystem url oprate_function deal_page.js
///used to read page file according to the path

///
const path_mod = require("path");

function get_page(path){
    console.log(path);
    const fs = require("fs");
 
    return new Promise(function(resolve, reject){
        fs.readFile(path, function(err, data){
            if(err){
                console.log("failed to read page file");
                let path = path_mod.resolve(BOOKSYSTEM, "./page/page_error.html")
                return reject(get_page(path));
            }
            else{
                // console.log(data.toString());
                return resolve(data.toString());
            }
        });
   });
}

function get_page_sync(path){
    console.log(path);
    const fs = require("fs");

    let data = fs.readFileSync(path).toString();
    // console.log(data);
    return data;
}

module.exports = {
    "get_page": get_page,
    "get_page_sync": get_page_sync,
}