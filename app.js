///booksystem app.js


///url_parser module used to parse url to related page
const url_parser = require("./url_parser");

///format module used to format time output
const format = require("./format/format");

///http module used to create server to listening some port
const http = require("http");

///bodyarser module used to parse post body
const bodyParser = require("koa-bodyparser");

///Koa class provide web frame
const Koa = require("koa");
const app = new Koa();

const session = require("koa-session");


///sql module used to connect database
const sql = require("./url/oprate_function/sql");

///global variable BOOKSYSTEM dir path
global.BOOKSYSTEM = __dirname;


console.log("book system start...");

///global variable connection
sql.connect();

app.keys = ["booksystem"];

app.use(session({}, app));

app.use(async function(ctx, next){
    console.log("book system request...");
    format.format_date();
 
    await next();
});

///middileware body parser used to parse form in post
app.use(bodyParser());

///midderware usr_parser (selfdifined) used to parser url to related page 
app.use(url_parser());

///create server and bind to port 3000
http.createServer(app.callback()).listen(5000);

///write the server start log
console.log("server running ar port 5000....");
