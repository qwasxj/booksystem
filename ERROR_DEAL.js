/// booksystem url oprate_function ERROR_DEAL.js

///used to deal with run error not 'error/request'
///used to file read error
///.........

///page_deal module used to read error page sent to client
const deal_page = require("./url/oprate_function/deal_page");

///path module used to get error page
const path_mod = require("path");

exports.ERROR_DEAL = async function(ctx, next){
    let path = path_mod.resolve(BOOKSYSTEM, "./page/page_error.html");

    ctx.response.type = "text/html";
    ctx.response.body = await deal_page.get_page(path);
}